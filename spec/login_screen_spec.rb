describe LoginScreen do
  tests LoginScreen

  def controller
    @controller ||= LoginScreen.new
  end

  alias :screen :controller

  it "is a PM::Screen" do
    screen.should.be.kind_of(PM::Screen)
  end

  it "has email input field" do
    view("Email").should.not == nil
  end

end
