describe HomeScreen do
  tests HomeScreen

  def controller
    @controller ||= HomeScreen.new(nav_bar: true)
  end

  alias :screen :controller

  it "is a PM::Screen" do
    screen.should.be.kind_of(PM::Screen)
  end

  it "has a title" do
    screen.title.should == "Home"
  end

end

