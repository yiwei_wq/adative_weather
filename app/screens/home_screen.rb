class HomeScreen < PM::Screen
  title "RAILS GUIDER"

  def on_load
    @layout = HomeLayout.new(root: self.view)
    @layout.build
  end

end

