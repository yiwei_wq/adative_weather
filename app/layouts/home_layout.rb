class HomeLayout < MK::Layout

  def layout
    root :main do
      add UIImageView, :image_view do
        constraints do
          center_x.equals(:superview)
          top.equals(:superview)
          bottom.equals(:text_container, :top).minus(20)
        end

        reapply do
          landscape do
            max_height.equals(:superview).times(0.1)
          end
        end
      end

      add UIView, :text_container do
        constraints do
          left.is == 0
          right.equals(:superview)
          bottom.equals(:superview)
        end
      end
    end
  end

  def main_style
    background_color  "#4a8df7".uicolor
  end

  def image_view_style
    image "cloud_small".uiimage
  end

  def text_container_style
    background_color "#3780BA".uicolor
  end

end
