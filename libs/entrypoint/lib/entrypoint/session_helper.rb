module EntryPoint
  module SessionHelper
    
    def first_time_run
      if NSUserDefaults['first_time_run'].nil?
        true 
      else
        NSUserDefaults['first_time_run'] = false
        false
      end
    end

  end
end

