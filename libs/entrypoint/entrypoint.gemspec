require File.expand_path('../lib/entrypoint/version.rb', __FILE__)

Gem::Specification.new do |s|
  s.name          = 'entrypoint'
  s.version       = EntryPoint::Version
  s.authors       = ["Yi Wei"]
  s.email         = 'yiwei.in.cyber@gmail.com'

  s.summary       = "Devise in RubyMotion"
  s.license       = "MIT"

  s.files         = Dir["lib/**/*"] + ["README.md"]
  s.test_files    = Dir["spec/**/*"]
  s.require_paths = ['lib']

  s.add_development_dependency "rake"
  s.add_development_dependency "motion-stump"

end
